#include "serveur.h"

#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>
#include <sys/stat.h>
#include <unistd.h>

#include "../shared/log.h"
#include "../shared/message.h"
#include "../shared/mq.h"
#include "../shared/result.h"
#include "service.h"

void log_result(const result_t r);

int main(int argc, char *argcv[]) { return start(); }

int start(void) {
  // TODO réécrire le signal handler pour les SIG_INT/TERM
  errno = 0;

  int msgid = init_MQ();          // au cas ou des clients aient fait des
  msgctl(msgid, IPC_RMID, NULL);  // tentatives abandonnées de connection

  msgid = init_MQ();
  if (errno != 0) {
    fprintf(stderr, "Erreur à la création de la message queue : %s\n",
            strerror(errno));
    exit(EXIT_FAILURE);
  } else {
    return process_connections(msgid);
  }
}

int process_connections(int msgid) {
  // on ne fait rien des processus fils après leurs terminaison
  signal(SIGCHLD, SIG_IGN);
  while (1) {
    mq_buffer buffer;
    log_info("listening...", NULL);

    if (msgrcv(msgid, &buffer, sizeof(buffer.mtext), MQ_TYPE_CLIENT, 0) > 0) {
      if (strcmp(MQ_CONNECT_REQUEST_CODE, buffer.mtext) == 0) {
        int id = fork();
        int client_id = client_counter++;
        if (id == 0) {
          // On charge le fils qui vient d'être créé de s'occuper
          // de la connexion avec le client demandant un service
          int ret = handle_connection(msgid, client_id);
          log_info("Fin du traitement connexion n°%d - code retour: %d",
                   client_id, ret);
          exit(ret);
        } else if (id < 0) {
          exit(EXIT_FAILURE);
        }
      }
    }
  }
  return EXIT_SUCCESS;
}

int handle_connection(int msgid, int client_id) {
  mq_buffer buffer;
  buffer.mtype = MQ_TYPE_SERVEUR;

  log_info("Traitement connexion n°%d", client_id);

  char pathname_read[M_MAX_SERVICE_NAME + 1];
  char pathname_write[M_MAX_SERVICE_NAME + 1];
  char pathnames[(M_MAX_SERVICE_NAME + 1) * 2];

  sprintf(pathname_read, "%s/read_%d%s", MQ_DIR, client_id,
          M_NAMED_PIPE_SUFFIX);
  sprintf(pathname_write, "%s/write_%d%s", MQ_DIR, client_id,
          M_NAMED_PIPE_SUFFIX);

  remove(pathname_read);
  remove(pathname_write);

  errno = 0;
  mkfifo(pathname_read, M_PERMISSION_FIFO);
  mkfifo(pathname_write, M_PERMISSION_FIFO);
  if (errno != 0) {
    log_error("Erreur à la création des fifos : %s", strerror(errno));
    exit(EXIT_FAILURE);
  }

  sprintf(pathnames, "%s%s%s", pathname_read, M_SEPARATOR, pathname_write);
  buffer.mtype = MQ_TYPE_SERVEUR;
  strcpy(buffer.mtext, pathnames);
  msgsnd(msgid, &buffer, sizeof(buffer.mtext), 1);

  int fd_read = open(pathname_read, O_RDONLY);
  int fd_write = open(pathname_write, O_WRONLY);

  char m_buffer[(M_MAX_SERVICE_NAME + 1) + (M_MAX_ARGS * (M_MAX_ARG + 1))];
  char r_buffer[(R_MAX_VALUE + 1) + (R_MAX_TIME_SPENT + 1) * 2];

  read(fd_read, m_buffer, sizeof(m_buffer));
  message_t m = message_new();

  m_deserialize(m, m_buffer);

  close(fd_read);
  unlink(pathname_read);

  result_t r = result_new();
  start_service(r, m);
  // log_result(r);
  message_free(m);

  r_serialize(r_buffer, r);
  write(fd_write, r_buffer, strlen(r_buffer));
  result_free(r);

  close(fd_write);
  unlink(pathname_write);
  errno = 0;
  return EXIT_SUCCESS;
}

// On retire les nouvelles lignes pour l'affichage du resultat dans le log
void log_result(const result_t r) {
  char tmp[R_MAX_VALUE + 1];
  strcpy(tmp, r->value);
  char *newline = strchr(tmp, '\n');
  while (newline != NULL) {
    *newline = ';';
    newline = strchr(tmp, '\n');
  }
  log_info("Resultat du traitement : \"%s\"", tmp);
}
